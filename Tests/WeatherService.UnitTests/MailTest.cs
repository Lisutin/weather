﻿using System.Net.Mail;
using AutoFixture;
using Moq;
using Xunit;
using WeatherService.Common;
using WeatherService.Common.Configuration;
using WeatherService.Common.Interfaces;


namespace WeatherService.UnitTests
{


    public class MailTest

    {       
       [Fact]
        public void MailAgentTest()
       {
            var fixture = new Fixture();

            var configuration = fixture.Build<ServiceConfiguration>()
               .With(t => t.AgentAddress, "weathersend@yandex.ru")
               .With(t => t.AgentSmtp, "smtp.yandex.ru")
               .With(t => t.AgentPassword, "Weathersend21")
               .Create();

           var smtpClientMock = new Mock<ISmtpClient>();
           smtpClientMock.Setup(t => t.Send(It.IsAny<MailMessage>()))
               .Returns(true);

            MailAgent ma = new MailAgent(configuration, smtpClientMock.Object);
            ma.AddRecipientAddress("a_lisutin@mail.ru");
            Assert.True(ma.SendMail("Тема письма3", "Тело письма3"));         

        }

    }

}
