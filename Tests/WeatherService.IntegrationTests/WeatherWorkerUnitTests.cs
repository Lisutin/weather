﻿using Microsoft.Extensions.Configuration;
using System.IO;
using WeatherService.Common.Configuration;
using WeatherService.YandexWeatherWorker;
using Xunit;


namespace WeatherService.IntegrationTests
{
    public class WeatherWorkerUnitTests
    {
        private readonly ServiceConfiguration _configuration;

        public WeatherWorkerUnitTests()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("configuration.json")
                .Build();
            _configuration = new ServiceConfiguration(config);
        }

        [Fact]
        public void WorkerTest()
        {

            var worker = new Worker(_configuration);
            worker.DoWork();
            //Проверка получения письма подписчиком
        }
    }
}
