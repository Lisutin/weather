using AutoFixture;
using Moq;
using WeatherService.Common;
using WeatherService.Common.Configuration;
using WeatherService.YandexWeatherWorker;
using Xunit;

namespace WeatherService.IntegrationTests
{
    public class WeatherProviderUnitTests
    {
        [Fact]
        public void ShouldGetWheatherInfoFromYandex()
        {
            var httpProvider = new HttpProvider();
            var fixture = new Fixture();
            var configuration = fixture.Build<ServiceConfiguration>()
                .With(t => t.RegionParamName, "region")
                .With(t => t.RegionParamValue, "191")
                .With(t => t.BaseUrl, @"https://export.yandex.ru")
                .With(t => t.Path, @"/bar/reginfo.xml")
                .Create();
            var weatherProvider = new WeatherProvider(httpProvider, configuration);
            var wheatherInfo = weatherProvider.GetWeatherInfo();
            Assert.NotNull(wheatherInfo);
        }
    }
}
