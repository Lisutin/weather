﻿using System;
using System.Net;
using System.Net.Mail;
using WeatherService.Common.Configuration;
using WeatherService.Common.Interfaces;

namespace WeatherService.Common
{
    public class MailClient : ISmtpClient
    {
        private static MailClient _instance;
        private readonly SmtpClient _client;

        private MailClient(ServiceConfiguration configuration)
        {
            _client = new SmtpClient();
            _client.Host = configuration.AgentSmtp;
            _client.Port = 587;
            _client.EnableSsl = true;
            _client.Credentials = new NetworkCredential(configuration.AgentAddress, configuration.AgentPassword);
            _client.DeliveryMethod = SmtpDeliveryMethod.Network;
        }

        public static MailClient GetInstance(ServiceConfiguration configuration)
        {
            return _instance ?? (_instance = new MailClient(configuration));
        }

        public bool Send(MailMessage message)
        {
            try
            {
                _client.Send(message);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}