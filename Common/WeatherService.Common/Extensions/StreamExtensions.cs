﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace WeatherService.Common.Extensions
{
    public static class StreamExtensions
    {
        public static T Deserialize<T>(this Stream stream)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            return (T)xmlSerializer.Deserialize(stream);
        }

        public static string ConvertToString(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

       
    }
}