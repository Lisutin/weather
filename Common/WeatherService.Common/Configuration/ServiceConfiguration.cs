﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace WeatherService.Common.Configuration
{
    public class ServiceConfiguration
    {
        public ServiceConfiguration() {}

        public ServiceConfiguration(IConfiguration configuration)
        {
            configuration.Bind("Service", this);
        }

        public string AgentSmtp { get; set; }
        public string AgentAddress { get; set; }
        public string AgentPassword { get; set; }
        public string BaseUrl { get; set; }
        public string Path { get; set; }
        public string TimeSend { get; set; }
        public string EndTimeSend { get; set; }
        public string RegionParamName { get; set; }
        public string RegionParamValue { get; set; }

        public IEnumerable<Subscriber> Subscribers { get; set; }
    }
}
