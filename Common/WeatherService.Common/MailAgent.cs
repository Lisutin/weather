﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using WeatherService.Common.Configuration;
using WeatherService.Common.Interfaces;

namespace WeatherService.Common
{
    public class MailAgent
    {
        private readonly ServiceConfiguration _configuration;
        private readonly ISmtpClient _client;

        private readonly List<string> _recipientAddresses;

        public MailAgent(ServiceConfiguration configuration, ISmtpClient client = null)
        { 
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _client = client ?? MailClient.GetInstance(configuration);
            _recipientAddresses = new List<string>();
        }

        public void AddRecipientAddress(string recipientAddress)
        {
            if (recipientAddress == null) throw new ArgumentNullException(nameof(recipientAddress));
            _recipientAddresses.Add(recipientAddress);
        }

        public bool SendMail(string mailText, string caption)
        {
            if (_recipientAddresses.Count == 0)
            {
                throw new ArgumentException("Пустой список адресатов!");
            }

            using (var mail = new MailMessage())
            {
                mail.From = new MailAddress(_configuration.AgentAddress);
                foreach (string addres in _recipientAddresses)
                {
                   mail.To.Add(new MailAddress(addres));
                }
                mail.Subject = caption;
                mail.Body = mailText;
                return _client.Send(mail);
            }
            
        }
    }
}

