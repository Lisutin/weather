﻿using System.IO;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using WeatherService.Common.Interfaces;

namespace WeatherService.Common
{
    public class HttpProvider : IHttpProvider
    {
        public async Task<Stream> Get(string baseUrl, string path, params QueryParam[] queryParams)
        {
            var url = baseUrl.AppendPathSegment(path);
            foreach (var queryParam in queryParams)
            {
                url.SetQueryParam(queryParam.Name, queryParam.Value);
            }

            return await url.GetStreamAsync();
        }
    }
}