﻿using System;

namespace WeatherService.Common.Models
{
    public class Weather
    {
        public DateTime Date { get; set; }
        public int Temperature { get; set; }
        public int Pressure { get; set; }
        public int WindSpeed { get; set; }
        public string WindDirection { get; set; }
    }
}
