﻿namespace WeatherService.Common.XmlModels
{
    public class YandexXmlWeatherModel
    {
        // Примечание. Для запуска созданного кода может потребоваться NET Framework версии 4.5 или более поздней версии и .NET Core или Standard версии 2.0 или более поздней.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class info
        {

            private infoRegion regionField;

            private object trafficField;

            private infoWeather weatherField;

            private string langField;

            /// <remarks/>
            public infoRegion region
            {
                get
                {
                    return this.regionField;
                }
                set
                {
                    this.regionField = value;
                }
            }

            /// <remarks/>
            public object traffic
            {
                get
                {
                    return this.trafficField;
                }
                set
                {
                    this.trafficField = value;
                }
            }

            /// <remarks/>
            public infoWeather weather
            {
                get
                {
                    return this.weatherField;
                }
                set
                {
                    this.weatherField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string lang
            {
                get
                {
                    return this.langField;
                }
                set
                {
                    this.langField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoRegion
        {

            private string titleField;

            private byte idField;

            private byte zoomField;

            private decimal latField;

            private decimal lonField;

            /// <remarks/>
            public string title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte zoom
            {
                get
                {
                    return this.zoomField;
                }
                set
                {
                    this.zoomField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal lat
            {
                get
                {
                    return this.latField;
                }
                set
                {
                    this.latField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal lon
            {
                get
                {
                    return this.lonField;
                }
                set
                {
                    this.lonField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeather
        {

            private string sourceField;

            private infoWeatherDay dayField;

            private infoWeatherUrl urlField;

            private byte climateField;

            private byte regionField;

            /// <remarks/>
            public string source
            {
                get
                {
                    return this.sourceField;
                }
                set
                {
                    this.sourceField = value;
                }
            }

            /// <remarks/>
            public infoWeatherDay day
            {
                get
                {
                    return this.dayField;
                }
                set
                {
                    this.dayField = value;
                }
            }

            /// <remarks/>
            public infoWeatherUrl url
            {
                get
                {
                    return this.urlField;
                }
                set
                {
                    this.urlField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte climate
            {
                get
                {
                    return this.climateField;
                }
                set
                {
                    this.climateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte region
            {
                get
                {
                    return this.regionField;
                }
                set
                {
                    this.regionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDay
        {

            private string titleField;

            private string countryField;

            private string time_zoneField;

            private byte summertimeField;

            private string sun_riseField;

            private string sunsetField;

            private string daytimeField;

            private infoWeatherDayDate dateField;

            private infoWeatherDayDay_part[] day_partField;

            private infoWeatherDayNight_short night_shortField;

            private infoWeatherDayTomorrow tomorrowField;

            private string[] textField;

            /// <remarks/>
            public string title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }

            /// <remarks/>
            public string country
            {
                get
                {
                    return this.countryField;
                }
                set
                {
                    this.countryField = value;
                }
            }

            /// <remarks/>
            public string time_zone
            {
                get
                {
                    return this.time_zoneField;
                }
                set
                {
                    this.time_zoneField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("summer-time")]
            public byte summertime
            {
                get
                {
                    return this.summertimeField;
                }
                set
                {
                    this.summertimeField = value;
                }
            }

            /// <remarks/>
            public string sun_rise
            {
                get
                {
                    return this.sun_riseField;
                }
                set
                {
                    this.sun_riseField = value;
                }
            }

            /// <remarks/>
            public string sunset
            {
                get
                {
                    return this.sunsetField;
                }
                set
                {
                    this.sunsetField = value;
                }
            }

            /// <remarks/>
            public string daytime
            {
                get
                {
                    return this.daytimeField;
                }
                set
                {
                    this.daytimeField = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayDate date
            {
                get
                {
                    return this.dateField;
                }
                set
                {
                    this.dateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("day_part")]
            public infoWeatherDayDay_part[] day_part
            {
                get
                {
                    return this.day_partField;
                }
                set
                {
                    this.day_partField = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayNight_short night_short
            {
                get
                {
                    return this.night_shortField;
                }
                set
                {
                    this.night_shortField = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayTomorrow tomorrow
            {
                get
                {
                    return this.tomorrowField;
                }
                set
                {
                    this.tomorrowField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string[] Text
            {
                get
                {
                    return this.textField;
                }
                set
                {
                    this.textField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDate
        {

            private infoWeatherDayDateDay dayField;

            private infoWeatherDayDateMonth monthField;

            private ushort yearField;

            private string daytimeField;

            private System.DateTime dateField;

            /// <remarks/>
            public infoWeatherDayDateDay day
            {
                get
                {
                    return this.dayField;
                }
                set
                {
                    this.dayField = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayDateMonth month
            {
                get
                {
                    return this.monthField;
                }
                set
                {
                    this.monthField = value;
                }
            }

            /// <remarks/>
            public ushort year
            {
                get
                {
                    return this.yearField;
                }
                set
                {
                    this.yearField = value;
                }
            }

            /// <remarks/>
            public string daytime
            {
                get
                {
                    return this.daytimeField;
                }
                set
                {
                    this.daytimeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public System.DateTime date
            {
                get
                {
                    return this.dateField;
                }
                set
                {
                    this.dateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDateDay
        {

            private string weekdayField;

            private byte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string weekday
            {
                get
                {
                    return this.weekdayField;
                }
                set
                {
                    this.weekdayField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public byte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDateMonth
        {

            private string nameField;

            private byte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public byte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_part
        {

            private string weather_typeField;

            private string weather_codeField;

            private string imageField;

            private infoWeatherDayDay_partImagev2 imagev2Field;

            private infoWeatherDayDay_partImagev3 imagev3Field;

            private infoWeatherDayDay_partTemperature_from temperature_fromField;

            private infoWeatherDayDay_partTemperature_to temperature_toField;

            private object image_numberField;

            private byte wind_speedField;

            private bool wind_speedFieldSpecified;

            private infoWeatherDayDay_partWind_direction wind_directionField;

            private byte dampnessField;

            private bool dampnessFieldSpecified;

            private ushort hectopascalField;

            private bool hectopascalFieldSpecified;

            private ushort torrField;

            private bool torrFieldSpecified;

            private ushort pressureField;

            private bool pressureFieldSpecified;

            private infoWeatherDayDay_partTemperature temperatureField;

            private string time_zoneField;

            private string observation_timeField;

            private System.DateTime observationField;

            private bool observationFieldSpecified;

            private byte typeidField;

            private string typeField;

            /// <remarks/>
            public string weather_type
            {
                get
                {
                    return this.weather_typeField;
                }
                set
                {
                    this.weather_typeField = value;
                }
            }

            /// <remarks/>
            public string weather_code
            {
                get
                {
                    return this.weather_codeField;
                }
                set
                {
                    this.weather_codeField = value;
                }
            }

            /// <remarks/>
            public string image
            {
                get
                {
                    return this.imageField;
                }
                set
                {
                    this.imageField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("image-v2")]
            public infoWeatherDayDay_partImagev2 imagev2
            {
                get
                {
                    return this.imagev2Field;
                }
                set
                {
                    this.imagev2Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("image-v3")]
            public infoWeatherDayDay_partImagev3 imagev3
            {
                get
                {
                    return this.imagev3Field;
                }
                set
                {
                    this.imagev3Field = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayDay_partTemperature_from temperature_from
            {
                get
                {
                    return this.temperature_fromField;
                }
                set
                {
                    this.temperature_fromField = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayDay_partTemperature_to temperature_to
            {
                get
                {
                    return this.temperature_toField;
                }
                set
                {
                    this.temperature_toField = value;
                }
            }

            /// <remarks/>
            public object image_number
            {
                get
                {
                    return this.image_numberField;
                }
                set
                {
                    this.image_numberField = value;
                }
            }

            /// <remarks/>
            public byte wind_speed
            {
                get
                {
                    return this.wind_speedField;
                }
                set
                {
                    this.wind_speedField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool wind_speedSpecified
            {
                get
                {
                    return this.wind_speedFieldSpecified;
                }
                set
                {
                    this.wind_speedFieldSpecified = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayDay_partWind_direction wind_direction
            {
                get
                {
                    return this.wind_directionField;
                }
                set
                {
                    this.wind_directionField = value;
                }
            }

            /// <remarks/>
            public byte dampness
            {
                get
                {
                    return this.dampnessField;
                }
                set
                {
                    this.dampnessField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool dampnessSpecified
            {
                get
                {
                    return this.dampnessFieldSpecified;
                }
                set
                {
                    this.dampnessFieldSpecified = value;
                }
            }

            /// <remarks/>
            public ushort hectopascal
            {
                get
                {
                    return this.hectopascalField;
                }
                set
                {
                    this.hectopascalField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool hectopascalSpecified
            {
                get
                {
                    return this.hectopascalFieldSpecified;
                }
                set
                {
                    this.hectopascalFieldSpecified = value;
                }
            }

            /// <remarks/>
            public ushort torr
            {
                get
                {
                    return this.torrField;
                }
                set
                {
                    this.torrField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool torrSpecified
            {
                get
                {
                    return this.torrFieldSpecified;
                }
                set
                {
                    this.torrFieldSpecified = value;
                }
            }

            /// <remarks/>
            public ushort pressure
            {
                get
                {
                    return this.pressureField;
                }
                set
                {
                    this.pressureField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool pressureSpecified
            {
                get
                {
                    return this.pressureFieldSpecified;
                }
                set
                {
                    this.pressureFieldSpecified = value;
                }
            }

            /// <remarks/>
            public infoWeatherDayDay_partTemperature temperature
            {
                get
                {
                    return this.temperatureField;
                }
                set
                {
                    this.temperatureField = value;
                }
            }

            /// <remarks/>
            public string time_zone
            {
                get
                {
                    return this.time_zoneField;
                }
                set
                {
                    this.time_zoneField = value;
                }
            }

            /// <remarks/>
            public string observation_time
            {
                get
                {
                    return this.observation_timeField;
                }
                set
                {
                    this.observation_timeField = value;
                }
            }

            /// <remarks/>
            public System.DateTime observation
            {
                get
                {
                    return this.observationField;
                }
                set
                {
                    this.observationField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool observationSpecified
            {
                get
                {
                    return this.observationFieldSpecified;
                }
                set
                {
                    this.observationFieldSpecified = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte typeid
            {
                get
                {
                    return this.typeidField;
                }
                set
                {
                    this.typeidField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_partImagev2
        {

            private string sizeField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string size
            {
                get
                {
                    return this.sizeField;
                }
                set
                {
                    this.sizeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_partImagev3
        {

            private byte sizeField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte size
            {
                get
                {
                    return this.sizeField;
                }
                set
                {
                    this.sizeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_partTemperature_from
        {

            private string class_nameField;

            private string colorField;

            private sbyte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string class_name
            {
                get
                {
                    return this.class_nameField;
                }
                set
                {
                    this.class_nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string color
            {
                get
                {
                    return this.colorField;
                }
                set
                {
                    this.colorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public sbyte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_partTemperature_to
        {

            private string class_nameField;

            private string colorField;

            private sbyte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string class_name
            {
                get
                {
                    return this.class_nameField;
                }
                set
                {
                    this.class_nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string color
            {
                get
                {
                    return this.colorField;
                }
                set
                {
                    this.colorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public sbyte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_partWind_direction
        {

            private string idField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayDay_partTemperature
        {

            private string class_nameField;

            private string colorField;

            private sbyte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string class_name
            {
                get
                {
                    return this.class_nameField;
                }
                set
                {
                    this.class_nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string color
            {
                get
                {
                    return this.colorField;
                }
                set
                {
                    this.colorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public sbyte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayNight_short
        {

            private infoWeatherDayNight_shortTemperature temperatureField;

            /// <remarks/>
            public infoWeatherDayNight_shortTemperature temperature
            {
                get
                {
                    return this.temperatureField;
                }
                set
                {
                    this.temperatureField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayNight_shortTemperature
        {

            private string class_nameField;

            private string colorField;

            private sbyte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string class_name
            {
                get
                {
                    return this.class_nameField;
                }
                set
                {
                    this.class_nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string color
            {
                get
                {
                    return this.colorField;
                }
                set
                {
                    this.colorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public sbyte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayTomorrow
        {

            private infoWeatherDayTomorrowTemperature temperatureField;

            /// <remarks/>
            public infoWeatherDayTomorrowTemperature temperature
            {
                get
                {
                    return this.temperatureField;
                }
                set
                {
                    this.temperatureField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherDayTomorrowTemperature
        {

            private string class_nameField;

            private string colorField;

            private sbyte valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string class_name
            {
                get
                {
                    return this.class_nameField;
                }
                set
                {
                    this.class_nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string color
            {
                get
                {
                    return this.colorField;
                }
                set
                {
                    this.colorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public sbyte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class infoWeatherUrl
        {

            private string slugField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string slug
            {
                get
                {
                    return this.slugField;
                }
                set
                {
                    this.slugField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }
    }
}