﻿using System.IO;
using System.Threading.Tasks;

namespace WeatherService.Common.Interfaces
{
    public interface IHttpProvider
    {
        Task<Stream> Get(string baseUrl, string path, params QueryParam[] queryParams);
    }
}