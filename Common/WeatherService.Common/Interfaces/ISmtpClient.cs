﻿using System.Net.Mail;

namespace WeatherService.Common.Interfaces
{
    public interface ISmtpClient
    {
        bool Send(MailMessage message);
    }
}