﻿using WeatherService.Common;
using WeatherService.Common.Configuration;
using WeatherService.Common.Models;

namespace WeatherService.YandexWeatherWorker
{
    public class Worker
    {
      
        private readonly ServiceConfiguration _configuration;
        

        public Worker(ServiceConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void DoWork()
        {
            HttpProvider httpProvider = new HttpProvider();
            WeatherProvider weatherProvider = new WeatherProvider(httpProvider, _configuration);
            MailAgent mailAgent = new MailAgent(_configuration);
           

            Weather weather = weatherProvider.GetWeatherInfo();
            var mailText = (weather.Date + " Температура  " + weather.Temperature + "  Давление  " + weather.Pressure +
                "  Скорость ветра  " + weather.WindSpeed + "  Направление ветра  " + weather.WindDirection);
            var caption = "Прогноз погоды";

            foreach(Subscriber subscriber in _configuration.Subscribers)
            {
                mailAgent.AddRecipientAddress(subscriber.Address);
            }
           

            mailAgent.SendMail(mailText, caption);
        }
    }
}
