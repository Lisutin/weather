﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Configuration;
using WeatherService.Common;
using WeatherService.Common.Configuration;
using WeatherService.Common.Extensions;
using WeatherService.Common.Interfaces;
using WeatherService.Common.Models;
using WeatherService.Common.XmlModels;


[assembly: InternalsVisibleTo("WeatherService.IntegrationTests")]
namespace WeatherService.YandexWeatherWorker
{
    
    internal class WeatherProvider
    {
        private readonly IHttpProvider _httpProvider;
        private readonly ServiceConfiguration _configuration;

        public WeatherProvider(IHttpProvider httpProvider, ServiceConfiguration configuration)
        {
            _httpProvider = httpProvider;
            _configuration = configuration;
        }

        public Weather GetWeatherInfo()
        {
            //TODO добавить забор параметров из конфигурации
            var queryParam = new QueryParam(_configuration.RegionParamName, _configuration.RegionParamValue);
            var result = _httpProvider.Get(_configuration.BaseUrl, _configuration.Path, queryParam).Result
                .Deserialize<YandexXmlWeatherModel.info>();

            return new Weather
            {
                Temperature = Convert.ToInt32(result.weather.day.day_part[0].temperature.Value),
                Pressure = Convert.ToInt32(result.weather.day.day_part[0].pressure),
                WindSpeed = Convert.ToInt32(result.weather.day.day_part[0].wind_speed),
                WindDirection = Convert.ToString(result.weather.day.day_part[0].wind_direction.Value),
                Date = result.weather.day.date.date
            };
        }
    }
}