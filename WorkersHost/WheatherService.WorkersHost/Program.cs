﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading;
using WeatherService.Common.Configuration;
using WeatherService.YandexWeatherWorker;

namespace WheatherService.WorkersHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("configuration.json")
                .Build();

            ServiceConfiguration serviceConfiguration = new ServiceConfiguration(config);
            Worker worker = new Worker(serviceConfiguration);

            while(true) {
                if(serviceConfiguration.TimeSend != null & serviceConfiguration.EndTimeSend != null)
                {
                    var start = TimeSpan.Parse(serviceConfiguration.TimeSend);
                    var end = TimeSpan.Parse(serviceConfiguration.EndTimeSend);
                    var now = DateTime.Now.TimeOfDay;

                    bool isInRange = start <= now && now <= end;

                    if (isInRange)
                      {
                        worker.DoWork();
                        Thread.Sleep(60000);
                      }
                                       
                }
            }

        }
    }
}
